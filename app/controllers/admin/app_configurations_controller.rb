class Admin::AppConfigurationsController < ApplicationController
  # GET /configurations/1/edit
  def edit

  end

  # PATCH/PUT /configurations/1
  # PATCH/PUT /configurations/1.json
  def update
    respond_to do |format|
      if app_configuration.update(configuration_params)
        format.html { redirect_to edit_admin_app_configuration_path(:prefeitura), notice: 'Configuration was successfully updated.' }
        format.json { render :show, status: :ok, location: app_configuration }
      else
        format.html { render :edit }
        format.json { render json: app_configuration.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def configuration_params
      params.require(:app_configuration).permit(:definitions => [:company_name, :slogan, :bottom_slogan, :address, :phone,
    :other_phone, :email])
    end
end
