class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :site_menu_items, :app_configuration

  def site_menu_items
    MenuItem.roots
  end

  def app_configuration
    AppConfiguration.find_uniq
  end
end
