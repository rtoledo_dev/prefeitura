class HomeController < ApplicationController
  def index
    @news_portal_transparencias = News.portal_transparencia.where(created_at: Date.today.beginning_of_year..Date.today.end_of_year)
  end
end
