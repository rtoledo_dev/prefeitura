module HomeHelper
  def portal_transparencias_dates(news)
    news.pluck(:created_at).map{|t| t.to_s(:db)}
  end
end
