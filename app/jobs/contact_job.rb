class ContactJob < ActiveJob::Base
  queue_as :default

  def perform(contact)
    ContactNotifier.public_form(contact).deliver_now
  end
end
