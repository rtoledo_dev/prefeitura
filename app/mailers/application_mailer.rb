class ApplicationMailer < ActionMailer::Base
  default from: "rodrigo@rtoledo.inf.br"
  layout 'mailer'
end
