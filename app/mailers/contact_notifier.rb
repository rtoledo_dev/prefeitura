class ContactNotifier < ApplicationMailer

  def public_form(contact)
    @contact = contact
    mail to: "to@example.org"
  end
end
