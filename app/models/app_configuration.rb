class AppConfiguration < ActiveRecord::Base
  self.table_name = "configurations"
  serialize :definitions

  def self.find_uniq
    app_configuration = AppConfiguration.first
    unless app_configuration
      app_configuration = AppConfiguration.new
      app_configuration.save
    end
    app_configuration
  end

  def company_name
    return I18n.t('app_title') if self.definitions.nil?
    self.definitions['company_name'].blank? ? I18n.t('app_title') : self.definitions['company_name']
  end
end
