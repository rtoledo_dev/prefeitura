class Contact < ActiveRecord::Base
  validates :name, :email, :message, presence: true
  after_create do
    ContactJob.perform_later self
  end
end
