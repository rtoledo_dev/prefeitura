class News < ActiveRecord::Base
  extend FriendlyId
  acts_as_taggable
  attr_accessor :master_picture

  friendly_id :title, use: :slugged
  belongs_to :news_category
  has_many :pictures, as: :imageable, dependent: :destroy

  validates :title, :content, :resume, :news_category_id, presence: true

  scope :portal_transparencia, -> {joins(:news_category).where("news_categories.slug = ?", "portal-transparencia")}

  def picture
    @picture ||= begin
      pictures.where(master: true).first.try(:image)
    end
  end

  after_save do
    if self.master_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, image: self.master_picture).save!
      end
    end
  end
end
