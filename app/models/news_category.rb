class NewsCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :name, presence: true
  has_many :news, dependent: :destroy
end
