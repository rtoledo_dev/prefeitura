class Picture < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  has_attached_file :image, styles: { front_small_list: '76x76#', front_medium: '600x398#', front_medium_vertical_list: "250x143#", front_horizontal_medium_list: '396x150#', big: '1600x972#' }, default_style: :big
  validates_attachment_content_type :image, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
