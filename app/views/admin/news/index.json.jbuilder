json.array!(@news) do |news|
  json.extract! news, :id, :news_category_id, :title, :slug, :resume, :content, :source
  json.url news_url(news, format: :json)
end
