set :output, "log/whenever.log"

every :day, :at => '00:10am' do
  runner "PreSale.send_pre_sale_emails_at_date"
  runner "PreSale.receive_pre_sale_emails_at_date"
end
