class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.integer :parent_id
      t.string :title
      t.string :link

      t.timestamps null: false
    end
  end
end
