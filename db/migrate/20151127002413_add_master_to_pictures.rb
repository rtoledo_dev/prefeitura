class AddMasterToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :master, :boolean
  end
end
