class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.references :news_category, index: true, foreign_key: true
      t.string :title
      t.string :slug
      t.text :resume
      t.text :content
      t.string :source

      t.timestamps null: false
    end
  end
end
