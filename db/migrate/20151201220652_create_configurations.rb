class CreateConfigurations < ActiveRecord::Migration
  def change
    create_table :configurations do |t|
      t.text :definitions

      t.timestamps null: false
    end
  end
end
