# Preview all emails at http://localhost:3000/rails/mailers/contact_notifier
class ContactNotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_notifier/public_form
  def public_form
    ContactNotifier.public_form
  end

end
